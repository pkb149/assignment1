FROM alpine:latest
ENV PROJECT=assignment
WORKDIR /usr/share/${PROJECT}

RUN apk update && \
apk upgrade && \
apk add --no-cache openjdk8-jre && \
apk add --no-cache curl && \
apk add --no-cache bash && \
apk add --no-cache procps && \
mkdir -p /usr/share/${PROJECT} && \
cd /usr/share/${PROJECT}

ARG JARFILE
ARG CONF

COPY $JARFILE /usr/share/${PROJECT}/application.jar
COPY $CONF /usr/share/${PROJECT}/configuration.yaml
COPY entrypoint.sh /usr/share/${PROJECT}/entrypoint.sh

ENTRYPOINT ["/bin/sh", "entrypoint.sh"]