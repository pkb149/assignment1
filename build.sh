mvn clean install
docker build -t bukukas:0.0.1 --network host --build-arg JARFILE=target/bukukas-1.0-SNAPSHOT.jar --build-arg CONF=src/main/resources/configuration.yaml .
docker image tag bukukas:0.0.2 registry.gitlab.com/pkb149/assignment1:0.0.2
docker push registry.gitlab.com/pkb149/assignment1:0.0.2
