from flask import Flask,request

app = Flask(__name__)


@app.route('/',methods = ['POST'])
def hello():
    if request.method == 'POST':
        print(request.data)
        print(request.json)


if __name__ == '__main__':
    app.run()