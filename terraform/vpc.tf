#
# VPC Resources
#  * VPC
#  * Subnets
#  * Internet Gateway
#  * Route Table
#

resource "aws_vpc" "bukukas" {
  cidr_block = "10.0.0.0/16"

  tags = tomap({
    "Name"                                      = "terraform-eks-bukukas-node",
    "kubernetes.io/cluster/${var.cluster-name}" = "shared",
  })
  enable_dns_hostnames = "true"
}

resource "aws_subnet" "bukukas" {
  count = 2

  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "10.0.${count.index}.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.bukukas.id

  tags = tomap({
    "Name"                                      = "terraform-eks-bukukas-node",
    "kubernetes.io/cluster/${var.cluster-name}" = "shared",
  })
}

resource "aws_internet_gateway" "bukukas" {
  vpc_id = aws_vpc.bukukas.id

  tags = {
    Name = "terraform-eks-bukukas"
  }
}

resource "aws_route_table" "bukukas" {
  vpc_id = aws_vpc.bukukas.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.bukukas.id
  }
}

resource "aws_route_table_association" "bukukas" {
  count = 2

  subnet_id      = aws_subnet.bukukas.*.id[count.index]
  route_table_id = aws_route_table.bukukas.id
}
