#
# EKS Worker Nodes Resources
#  * IAM role allowing Kubernetes actions to access other AWS services
#  * EKS Node Group to launch worker nodes
#

resource "aws_iam_role" "bukukas-worker-role" {
  name = "terraform-eks-bukukas-node"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "bukukas-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.bukukas-worker-role.name
}

resource "aws_iam_role_policy_attachment" "bukukas-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.bukukas-worker-role.name
}

resource "aws_iam_role_policy_attachment" "bukukas-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.bukukas-worker-role.name
}

resource "aws_security_group" "sg-public-instance" {
  name = "nodejs_sg"
  vpc_id = aws_vpc.bukukas.id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 30807
    to_port = 30807
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 31073
    to_port = 31073
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eks_node_group" "bukukas" {
  cluster_name    = aws_eks_cluster.bukukas-cluster.name
  node_group_name = "bukukas-cluster"
  node_role_arn   = aws_iam_role.bukukas-worker-role.arn
  subnet_ids      = aws_subnet.bukukas[*].id
  instance_types = ["t2.small"]

  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }

  remote_access {
    ec2_ssh_key = var.key_name
    source_security_group_ids = [aws_security_group.sg-public-instance.id]
  }

  depends_on = [
    aws_iam_role_policy_attachment.bukukas-node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.bukukas-node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.bukukas-node-AmazonEC2ContainerRegistryReadOnly,
  ]
}

