variable "aws_region" {
  default = "us-west-2"
}

variable "cluster-name" {
  default = "bukukas-assignment"
  type    = string
}

variable "name" {
  default = "bukukas-assignment"
  type    = string
}

variable "key_name" {
  default = "bukukas"
}

variable "private_key_path" {}