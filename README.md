##### App has below 3 endpoints, accessible at temp aws elb url: 
 
 Temp elb: http://aa3855070543d4fd7bd9022e28471748-331474989.us-west-2.elb.amazonaws.com

1. Set endpoint:
```
curl --location --request POST 'http://hostname/v1/cache/set' \
   --header 'Content-Type: application/x-www-form-urlencoded' \
   --data-urlencode 'key=abscond' \
   --data-urlencode 'value=5'
```


2. get endpoint:
```
curl --location --request GET 'http://hostname/v1/cache/get/<key>'
```


3. search endpoint:
```
curl http://hostname/v1/cache/search/<key>
```


It has a default cache size of 100 specified via configuration file. 




NOTE: keeping replica count to only one, since this is in memory cache, and doesn't colaborate with other pods. 
Only work as a standalone application, Hence adding two pod results in inconsistent response. 


metrics conversion:
https://www.robustperception.io/exposing-dropwizard-metrics-to-prometheus 

chose to expose using dropwizard metrics and convert to prometheus format, which has disadvantages too. 
If left with time, will expose directly using prometheus exporters.


prometheus exporter here:
http://aa3855070543d4fd7bd9022e28471748-331474989.us-west-2.elb.amazonaws.com/metrics


please ignore build.sh and deploy.sh files. These files were created only for my reference. 
