package com.bukukas.assignment.cacheBuilder;

import org.junit.Test;

/**
 * created by prashant.bh on 05/11/21
 */
public class TrieTest {
    @Test
    public void TrieGetTest() {
        Trie.TrieNode root = new Trie.TrieNode();
        Trie trie = new Trie(root);
        trie.getOrInsert("apple","1");
        trie.getOrInsert("abs","2");
        trie.getOrInsert("absolute","3");
        trie.getOrInsert("abdomen","4");
        trie.getOrInsert("abscond","4");
        assert trie.get("apple").key.equals("apple");
        assert trie.get("app")==null;
    }

    @Test
    public void TrieSearchTest(){
        Trie.TrieNode root = new Trie.TrieNode();
        Trie trie = new Trie(root);
        trie.getOrInsert("apple","1");
        trie.getOrInsert("abs","2");
        trie.getOrInsert("absolute","3");
        trie.getOrInsert("abdomen","4");
        trie.getOrInsert("abscond","4");
        assert trie.search("app").size()==1;
        assert trie.search("ab").size()==4;
        assert trie.get("applee")==null;
    }

    @Test
    public void TrieDeleteTest1(){
        Trie.TrieNode root = new Trie.TrieNode();
        Trie trie = new Trie(root);
        trie.getOrInsert("apple","1");
        trie.getOrInsert("abs","2");
        trie.getOrInsert("absolute","3");
        trie.getOrInsert("abdomen","4");
        trie.getOrInsert("abscond","4");
        assert trie.search("app").size()==1;
        assert trie.search("ab").size()==4;
        assert trie.remove("apple");
        assert trie.get("apple")==null;
    }


    @Test
    public void TrieDeleteTest2(){
        Trie.TrieNode root = new Trie.TrieNode();
        Trie trie = new Trie(root);
        trie.getOrInsert("apple","1");
        trie.getOrInsert("abs","2");
        trie.getOrInsert("absolute","3");
        trie.getOrInsert("abdomen","4");
        trie.getOrInsert("abscond","4");
        assert trie.search("app").size()==1;
        assert trie.search("ab").size()==4;
        assert trie.remove("abs");
        assert trie.get("abs")==null;
        assert trie.search("abs").size()==2;
        assert trie.get("abscond").key.equals("abscond");
    }
}
