package com.bukukas.assignment.cacheBuilder;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;

import java.util.Set;

import static com.codahale.metrics.MetricRegistry.name;

/**
 * created by prashant.bh on 04/11/21
 */
public class Cache {
    Trie trie = new Trie(new Trie.TrieNode());
    Trie reverseTrie = new Trie(new Trie.TrieNode());
    LinkedList list = new LinkedList();
    int capacity;
    int currentUsage = 0;
    LinkedListNode last;
    static Counter counter;

    public Cache(int capacity, MetricRegistry registry){
        counter = registry.counter(name(Cache.class, "cache_size"));
        registry.register("cache_size",counter);
        this.capacity=capacity;
        list.head= new LinkedListNode(null,"head");
        last=list.head.next=new LinkedListNode(null,"last");
        list.head.previous=null;
    }

    class LinkedList {
        LinkedListNode head;
    }

    public String get(String key){
        LinkedListNode node = trie.get(key);
        if(node==null){
            return null;
            // key doesn't exist in cache
        }else{
            //removing node from current position and correcting connections
            LinkedListNode next = node.next;
            LinkedListNode previous = node.previous;
            previous.next=next;
            next.previous=previous;
            //inserting node at h1 and correcting connections
            LinkedListNode h1= list.head.next;
            list.head.next=node;
            node.previous=list.head;
            node.next=h1;
            h1.previous=node;
            return node.data;
        }
    }


    public String search(String key){
        Set<LinkedListNode>  listNodes = trie.search(key);
        Set<LinkedListNode> listNodesReverse = reverseTrie.search(new StringBuilder(key).reverse().toString());
        if(listNodes!=null && listNodesReverse!=null) {
            String str="";
            listNodes.addAll(listNodesReverse);
            for (LinkedListNode node: listNodes) {
                str = str+ node.key+",";
            }
            return str;
        }
        else if(listNodes!=null && listNodesReverse==null) {
            String str="";
            for (LinkedListNode node: listNodes) {
                str = str+ node.key+",";
            }
            return str;
        }
        else if(listNodes==null && listNodesReverse!=null) {
            String str="";
            for (LinkedListNode node: listNodesReverse) {
                str = str+ node.key+",";
            }
            return str;
        }else{
            return null;
        }
    }

    public String set(String key, String str){
        //1. check if exists, if yes.. then update by deleting and inserting and updating value during insertion.
        //2. if doesn't exist then? check if size< capacity
        //2a. if yes, insert at head
        //2b. if no, remove the last and insert at head
        LinkedListNode node = trie.get(key);
        if(node!=null){
            //key already exist
            node.flag=true;
            LinkedListNode next = node.next;
            LinkedListNode previous = node.previous;
            previous.next=next;
            next.previous=previous;

            //udpating data

            node.data=str;

            //inserting node at h1 and correcting connections
            LinkedListNode h1= list.head.next;
            list.head.next=node;
            node.previous=list.head;
            node.next=h1;
            h1.previous=node;
            node.flag=false;
            return "Already Exists. Updated";
        }
        else{
            //key doesn't exist
            if(currentUsage<capacity){
                node = trie.getOrInsert(key,str);
                reverseTrie.getOrInsertWithGivenNode(new StringBuilder(key).reverse().toString(),node);
                //inserting at h1
                LinkedListNode h1= list.head.next;
                list.head.next=node;
                node.previous=list.head;
                node.next=h1;
                h1.previous=node;
                currentUsage++;
                counter.inc();
                return "Inserted";
            }else{
                //a logic to remove the element from last
                //remove the key from map
                //add an element after head
                // add it to hashmap
                LinkedListNode t1 = last.previous;
                LinkedListNode t2= t1.previous;
                t2.next=t1.next;
                last.previous=t2;

                trie.remove(t1.key);

                t1.next=null;
                t1.previous=null;
                t1=null;

                node=trie.getOrInsert(key,str);
                reverseTrie.getOrInsertWithGivenNode(new StringBuilder(key).reverse().toString(),node);
                LinkedListNode h1= list.head.next;
                list.head.next=node;
                node.previous=list.head;
                node.next=h1;
                h1.previous=node;
                return "Inserted, replacing least recently used key";
            }
        }
    }
}
