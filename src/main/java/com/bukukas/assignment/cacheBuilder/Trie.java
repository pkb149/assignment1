package com.bukukas.assignment.cacheBuilder;

import java.util.*;

/**
 * created by prashant.bh on 05/11/21
 */
public class Trie {
    static class TrieNode{
        Map<Character,TrieNode> children = new HashMap<Character,TrieNode>();
        boolean isEndOfWord;
        LinkedListNode node;
        TrieNode(){
            isEndOfWord = false;
        }
    }
    Trie(TrieNode root){
        this.root=root;
    }

    TrieNode root;

    LinkedListNode getOrInsert(String key, String value)
    {
        TrieNode current = root;
        int length = key.length();
        for (int i=0;i<length;i++){
            if(current.children==null || !current.children.containsKey(key.charAt(i))){
                current.children.put(key.charAt(i),new TrieNode());
            }
            current = current.children.get(key.charAt(i));
        }
        //key already exists
        if(current.isEndOfWord){
            return current.node;
        }
        //key doesn't exist
        current.isEndOfWord=true;
        current.node = new LinkedListNode(key,value);
        return current.node;
    }

    LinkedListNode getOrInsertWithGivenNode(String key, LinkedListNode node)
    {
        TrieNode current = root;
        int length = key.length();
        for (int i=0;i<length;i++){
            if(current.children==null || !current.children.containsKey(key.charAt(i))){
                current.children.put(key.charAt(i),new TrieNode());
            }
            current = current.children.get(key.charAt(i));
        }
        //key already exists
        if(current.isEndOfWord){
            return current.node;
        }
        //key doesn't exist
        current.isEndOfWord=true;
        current.node = node;
        return current.node;
    }

    LinkedListNode get(String key) {
        TrieNode current = root;
        int length = key.length();
        for (int i = 0; i < length; i++) {
            if (current.children.containsKey(key.charAt(i))) {
                current = current.children.get(key.charAt(i));
            } else {
                return null;
            }
        }
        if (current.isEndOfWord) {
            return current.node;
        }
        return null;
    }

    Set<LinkedListNode> search(String key) {
        TrieNode current = root;
        int length = key.length();
        for (int i = 0; i < length; i++) {
            if (current.children.containsKey(key.charAt(i))) {
                current = current.children.get(key.charAt(i));
            } else {
                return null;
            }
        }
        Set<LinkedListNode> set = new HashSet<>();
        return iterate(set,current);
    }

    Set<LinkedListNode> iterate(Set<LinkedListNode> list, TrieNode trieNode){
        if(trieNode.isEndOfWord){
            list.add(trieNode.node);
        }
        if(trieNode.children == null){
            return list;
        }
        for (Map.Entry<Character,TrieNode> child : trieNode.children.entrySet()) {
            iterate(list,child.getValue());
        }
        return list;
    }

    boolean remove(String key) {
        TrieNode current = root;
        int length = key.length();
        Map<TrieNode,TrieNode> parentMap = new HashMap<TrieNode,TrieNode>();
        LinkedList<TrieNode> node = new LinkedList<>();

        for (int i = 0; i < length; i++) {
            if (current.children.containsKey(key.charAt(i))) {
                TrieNode children = current.children.get(key.charAt(i));
                parentMap.put(children,current);
                current = children;
            } else {
                return false;
            }
        }
        if (!current.isEndOfWord) {
            return false;
        }
        //if it has children, only mark as not end of word end exit successfully
        if(current.children.size()>0){
          current.isEndOfWord=false;
          current.node=null;
          return true;
        }
        int i;
        // if it doesn't have children, then delete upper connections too, till we find some node with flag isEndOfWord true.
        for (i = length-1; i >= 0; i--) {
            TrieNode parent = parentMap.get(current);
            current = parent;
            //1. remove root to first connection, enrich parentMap for that.
            //2. children count more than 1, exit
            if(i==0 || parentMap.get(current).children.size()>1 || parentMap.get(parent).isEndOfWord==true){
                break;
            }
        }
        parentMap.get(current).children.remove(key.charAt(i-1));
        return true;
    }
}
