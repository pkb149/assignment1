package com.bukukas.assignment;

import com.bukukas.assignment.config.CustomConfig;
import io.dropwizard.Configuration;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * created by prashant.bh on 04/11/21
 */

public class ApplicationConfiguration extends Configuration {
    @Valid
    @NotNull
    private CustomConfig customConfig;

    public CustomConfig getCustomConfig() {
        return customConfig;
    }
}

