package com.bukukas.assignment.resources;

import com.bukukas.assignment.cacheBuilder.Cache;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.ResponseMetered;
import com.codahale.metrics.annotation.Timed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/v1/cache")
public class AssignmentResource {
    private Cache cache;
    public AssignmentResource(Cache cache) {
        this.cache = cache;
    }

    @GET
    @Path("/get/{key}")
    @Produces(value = MediaType.APPLICATION_JSON)
    @Timed
    @Metered(name = "getMeter")
    @ResponseMetered
    public Response getValueAtAKey(@PathParam("key") String key){
        String result = cache.get(key);
        if(result==null){
            return Response.status(Response.Status.NOT_FOUND).entity("Key doesn't exist").build();
        }else{
            return Response.ok().entity(result).build();
        }

    }

    @POST
    @Path("/set")
    @Produces(value = MediaType.APPLICATION_JSON)
    @Timed
    @Metered(name = "addMeter")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @ResponseMetered
    public Response setValueOfAKey(@FormParam("key") String key, @FormParam("value") String value){
        return Response.ok().entity(cache.set(key,value)).build();
    }

    @GET
    @Path("/search/{query}")
    @Produces(value = MediaType.APPLICATION_JSON)
    @Timed
    @Metered(name = "searchMeter")
    @ResponseMetered
    public Response searchKeys(@PathParam("query") String query){
        String result = cache.search(query);
        if(result==null){
            return Response.status(Response.Status.NOT_FOUND).entity("no match found").build();
        }else{
            return Response.ok().entity(result).build();
        }
    }
}