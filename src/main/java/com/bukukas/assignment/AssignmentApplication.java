package com.bukukas.assignment;

import com.bukukas.assignment.cacheBuilder.Cache;
import com.bukukas.assignment.healthCheck.Health;
import com.bukukas.assignment.resources.AssignmentResource;
import com.codahale.metrics.MetricRegistry;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator;
import io.opentelemetry.context.propagation.ContextPropagators;
import io.opentelemetry.exporter.jaeger.JaegerGrpcSpanExporter;
import io.opentelemetry.exporter.otlp.trace.OtlpGrpcSpanExporter;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.export.BatchSpanProcessor;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.MetricsServlet;
import io.prometheus.client.dropwizard.DropwizardExports;

import java.util.concurrent.TimeUnit;

public class AssignmentApplication extends Application<ApplicationConfiguration> {
    @Override
    public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {
        super.initialize(bootstrap);
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor())
        );
    }

    public static void main(String[] args) throws Exception {
        new AssignmentApplication().run(args);
    }

    public void run(ApplicationConfiguration applicationConfiguration, Environment environment) throws Exception {
        int capacity = applicationConfiguration.getCustomConfig().getCacheCapacity();
        environment.healthChecks().register("template", new Health());

        final MetricRegistry registry = environment.metrics();

        //no need to expose metrics using jmx
        //SharedMetricRegistries.add("metric-registry", registry);
        //JmxReporter.forRegistry(registry).build().start();
        //CollectorRegistry.defaultRegistry.register(new DropwizardExports(registry));
        CollectorRegistry collectorRegistry = new CollectorRegistry();
        collectorRegistry.register(new DropwizardExports(registry));
        environment.admin().addServlet("metrics", new MetricsServlet(collectorRegistry)).addMapping("/metrics");


        JaegerGrpcSpanExporter jaegerExporter = JaegerGrpcSpanExporter.builder()
                .setEndpoint("http://localhost:3336")
                .setTimeout(30, TimeUnit.SECONDS)
                .build();

        SdkTracerProvider sdkTracerProvider = SdkTracerProvider.builder()
                .addSpanProcessor(BatchSpanProcessor.builder(jaegerExporter).build())
                .build();

        OpenTelemetry openTelemetry = OpenTelemetrySdk.builder()
                .setTracerProvider(sdkTracerProvider)
                .setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()))
                .buildAndRegisterGlobal();
        Tracer tracer = openTelemetry.getTracer("instrumentation-library-name", "1.0.0");

        Cache cache = new Cache(capacity,registry);
        environment.jersey().register(new AssignmentResource(cache));

    }

}
