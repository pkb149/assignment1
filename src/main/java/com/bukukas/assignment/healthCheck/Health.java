package com.bukukas.assignment.healthCheck;

import com.codahale.metrics.health.HealthCheck;

/**
 * created by prashant.bh on 05/11/21
 */

public class Health extends HealthCheck
{
    @Override
    protected Result check() throws Exception
    {

        return Result.healthy();

        //return Result.unhealthy("Error message");
    }
}