#!/bin/bash
graceful_shutdown (){
    echo "received sigkill/sigterm"
    echo "APP_PID: $APP_PID"
    kill -TERM "$child" 2>/dev/null
}

trap graceful_shutdown SIGTERM


java -jar application.jar server configuration.yaml &
APP_PID=$!
wait "$APP_PID"
echo "APP_PID: $APP_PID"